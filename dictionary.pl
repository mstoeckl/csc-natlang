
% real English is complicated. But here we live in a world of girls, boys, 
% governments, and watermelons, all liking, running, and conscripting.
% No negation. It's a happy world, with the occasional evil:\ Any "any" will
% never make it here.


% It's faster to hard-code the language facts than to try tricks like C/M's 
% is_noun.

% unicode accepts prolog: could use  ∃, ∀, ¬∃
determiner(vowel,sing,exists) --> [an].% an aardvark does
determiner(conso,sing,exists) --> [a].% a cat does
determiner(_,sing,exists) --> [the].% the dog does
determiner(_,plur,forall) --> [the]; [all].% the cats do = all cats do
determiner(_,_,exists) --> [some].% some cats do; some cat does
determiner(_,plur,forall) --> []. % cats do

who_rel --> [who]; [that]; [which].
whom_rel --> [whom]; [that]; [which].

noun(conso, sing, noun(boy)) --> [boy].
noun(conso, plur, noun(boy)) --> [boys].
noun(conso, sing, noun(girl)) --> [girl].
noun(conso, plur, noun(girl)) --> [girls].
noun(conso, sing, noun(watermelon)) --> [watermelon].
noun(conso, plur, noun(watermelon)) --> [watermelons].
noun(vowel, sing, noun(apple)) --> [apple].
noun(vowel, plur, noun(apple)) --> [apples].
noun(conso, sing, noun(government)) --> [government].
noun(conso, plur, noun(government)) --> [governments].
noun(conso, plur, noun(person)) --> [people].

tverb(sing, verb(conscript)) --> [conscripts].
tverb(plur, verb(conscript)) --> [conscript].
tverb(sing, verb(like)) --> [likes].
tverb(plur, verb(like)) --> [like].

iverb(sing, verb(run)) --> [runs].
iverb(plur, verb(run)) --> [run].

link(sing, be_verb) --> [is].
link(plur, be_verb) --> [are].

adj(vowel,adj(evil)) --> [evil].

punctuation --> ['.'].
