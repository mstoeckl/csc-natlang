#!/usr/bin/python

from random import choice, random, randint, sample, shuffle
from subprocess import check_output, STDOUT
from os import system
from sys import stderr
from collections import defaultdict
from math import log

cap = 200

def union(*x):
    u = x[0]
    for j in x[1:]:
        u = u.union(j)
    return u

def standard():
    plurables = {"run", "like","conscript","apple","boy","girl",
             "government","watermelon"}
    dps = set(x + "s"  for x in plurables)
    common = {"a","the","an"}
    middling = {"all","some","is","are","evil","who","that","whom","which","people"}
    wds = list(dps) + list(plurables) + list(common) + list(middling) + ["is"] + ["is"] + ["is"]
    shuffle(wds)
    return wds

words = standard()
print(words)

def generate_phrase():
    le = int(1/(0.1+random()))
    return [choice(words) for _ in range(le)]

def randswap(seq,val):
    p = randint(0,len(seq)-1)
    return seq[:p-1] + [val] + seq[p:]

def scales(iterator):
    bins = defaultdict(int)
    l = 0
    for word in iterator:
        bins[word] += 1
        l += 1
    for word in bins:
        bins[word] = l / bins[word]
    return bins

def mutate(phrase):
    y = random()
    if y < 0.2:
        # insert consecutive
        c = randint(1,4)
        p = randint(0,len(phrase)-1)
        insert = [choice(words) for _ in range(c)]
        return phrase[:p] + insert + phrase[p:]
    elif y < 0.4:
        # delete consecutive
        c = randint(1,6)
        p = randint(0,len(phrase)-1)
        return phrase[:p] + phrase[p+c:]
    else:
        # replace random
        for _ in range(randint(1,3)):
            phrase = randswap(phrase, choice(words))
        return phrase

def compile_cpof(target):
    system("swipl --stand_alone=true -o {} -g {} -c cpof.pl".format(target,target))

def response(name, trash):
    command = "echo \"{}\" | ./{}".format(trash,name)
    #print(command)
    return check_output(command, stderr=STDOUT,shell=True).decode("utf8")

def sentencify_input(s):
    return (" ".join(s) + ".").lower()

def get_food(name):
    food = []
    with open(name+".success","r") as inp:
        for l in inp.readlines():
            food.append(list(filter(None,l[:-2].split(" "))))
    return food

def generate_input(name):
    food = get_food(name)
    # keep what we have
    print("\ncopy\n",file=stderr)
    for f in food:
        yield sentencify_input(f)
    # mutate existing sentences
    print("\nmutate\n",file=stderr)
    for _ in range(2):
        for f in food:
            yield sentencify_input(mutate(f))
    # combine sentences
    print("\ngraft\n",file=stderr)
    for f in food:
        graft = choice(food)
        i = randint(0,len(graft) - 2)
        graft = graft[i:]
        j = randint(1,len(graft) -  1)
        graft = graft[:j]
        k = randint(0, len(f) - 1)
        yield sentencify_input(f[:k] + graft + f[k:])
        
    # generate de novo
    print("\nspawn\n",file=stderr)
    for _ in range(100):
        yield sentencify_input(generate_phrase())

def entropy(stream, scales):
    u = list(stream)
    s = defaultdict(int)
    for x in u:
        s[x] += 1
    freqs = [s[w]*scales[w] for w in s]
    j = 0
    for k in freqs:
        if k:
            j += log(k) * k
    return j / len(u)
    

def test_read():
    name = "test_read"
    compile_cpof(name)
    with open("test_read.success","w") as f:
        for _ in range(100):
            s = sentencify_input(generate_phrase())
            r = response(name, s)
            print(r)
            f.write(r)

def sub_parse(name):
    good = set()
    for i,s in enumerate(generate_input(name)):
        if i % 10 == 0:
            print(i)
        r = response(name, s)
        if r: # failure is silent
            good.add(s)
            #print("Live:", s)
        else:
            pass
            #print("Die:", s)
    if good:
        frequencies = scales(w for s in good for w in filter(None,s[:-1].split()))
        filt = sorted(good, key=lambda s: entropy(filter(None, s[:-1].split()), frequencies))
        # only care about best
        filt = filt[-cap:]
        #order = sorted(filt,key=lambda x:len(x))
        order = filt
        for word in sorted(words, key=lambda w: frequencies[w]):
            print(frequencies[word], word)
        with open(name+".success","w") as f:
            for s in order:
                #print(entropy(filter(None, s[:-1].split()), frequencies), s)
                f.write(s+"\n")

def test_parse(inf=False):
    name = "test_parse"
    compile_cpof(name)
    sub_parse(name)
    while inf:
        sub_parse(name)

def test_translate():
    name = "test_translate"
    compile_cpof(name)
    # fixme just read input.
    with open("test_translate.success","w") as f:
        for i, s in enumerate(sentencify_input(x) for x in get_food("test_parse")):
            if i % 10 == 0:
                print(i)
            r = response(name, s)
            if not r:
                print(s)
            f.write(r)

test_read()
test_translate()
test_parse(True)


