# Why?

For the 2015 University of Rochester course, Computation and Formal Systems.

# How to Run

To run the program, execute

    swipl cpof.pl

and to test the code, use in the prolog prompt

    read_in(V), parse(V,X).

and

    read_in(V), translate(V,X).

Both of these will ask for a period-terminated sentence of standard input, and 
return (in `X`), the parse and translation trees.

# Description of the Problem that was Solved

There are two tasks. The first is to create a prolog predicate `parse(+A, -B)`
that takes a comma-delimited symbol sentence, like

    [the,boy,whom,the,girl,likes,likes,the,girl,whom,the,boy,likes,'.']

with the period in quotes (`'.'` ) to differentiate it as a symbol. `parse` then
yields the tree construction corresponding to the the input string with an
alphabet of symbols. In this case, giving (expanded form)

    sentence(noun_phrase(exists,
                         noun(boy),
                         rel_object_cl(noun_phrase(exists,
                                                   noun(girl)),
                                       verb(like))),
             verb_phrase(verb(like),
                         noun_phrase(exists,
                                     noun(girl),
                                     rel_object_cl(noun_phrase(exists,
                                                               noun(boy)),
                                                   verb(like)))))

The second task is to create a prolog predicate `translate(+A, -B)` with
the same input but that outputs a translation of the input into first-order
predicate calculus. With the same input, this would be (expanded again):

    exists(x1,
           boy(x1) &
            exists(x4,
                   girl(x4) &
                     like(x4,x1)) &
            exists(x2,
                   girl(x2) & 
                     exists(x3,
                            boy(x3) &
                              like(x3,x2)) &
                     like(x1,x2)))

The language that this program handles is defined by

    Noun = {apple, apples, boy, boys, girl, girls, government, governments,
            watermelon, watermelons, people}
    Det = {a, an, the, some, all}
    Verb = {conscript, conscripts, like, likes, run, runs}
    BeVerb = {is, are}
    Adj = {evil}
    Rel = {that, whom, who, which}.

# Description of Implementation and Approach

The predicate `read_in(-A)`, defined in `read_in.pl`, reads a sentence from
standard input an converts it into the form that both `translate` and `parse`
take as input.

The predicate `parse` was designed to cover all sentences in the provided test
case, as well as cases in which adjectives are applied to nouns. The grammar
that `parse` matches is as follows (copied and abridged from the code).

    S --> NP VP terminal
    NP --> DET ANOUN WHOM_CL
    NP --> DET ANOUN WHO_CL
    NP --> DET ANOUN
    WHOM_CL --> whom_rel NP transverb
    WHO_CL --> who_rel VP
    VP --> transverb
    VP --> transverb NP
    VP --> intransverb
    VP --> linkverb adj
    VP --> linkverb NP
    ANOUN --> noun
    ANOUN --> adj ANOUN

The structure of the parse tree is built in parallel as the sentence is 
recognized. Two additional variables are passed along with the parser to ensure
number and initial-sound agreement. (So, `an apple` is legal, `an apples` and
`a apple` aren't.) For instance, the code uses 

    anoun(E,A,X) --> noun(E,A,X).
    anoun(E,A,modified_noun(X,Y)) --> adj(E,X),anoun(_,A,Y).
    
to match the last constructions in the above grammar. The variable `E` exists
to match the starting sound of the phrase, and `A` exists to match the number
of objects referred to. The code thus requires that the starting sound of the 
`ANOUN` nonterminal is the same as that of the the first word it contains, and 
that its number corresponds to the number of the described noun terminal.

The language that `parse` matches is defined in `dictionary.pl`. Additional 
extensions to the language can be defined by including further, similar
dictionary files. The dictionary is, to a large extend, hard coded because
a general solution is overkill. For example, 

    determiner(_,plur,forall) --> [].

defines that no prefix indicates universal quantification no matter what the 
leading sound of the following term is.

`translate` is defined to first `parse` the sentence it is given, and then 
rewrite the provided parse tree, in the predicate named `convert`. The code
for `convert` is complicated, relying on the helper predicate `ns(-A)` to
generate a new symbol, and the predicate `sc(+V,+A,-O)` which converts a 
subexpression `A` parameterized over the variable `V` to FOPC form `O`. A simple
example from the code is

    sc(Var, modified_noun(adj(U),V), A & B) :- sc(Var, V, A), B =.. [U, Var].
    
which rewrites the adjective `U` attached to the term `V` as the conjunction of
the predicate expressing `U` applied to to whatever variable the entire input
refers to, with the predicate or conjunction of predicates that the term `V`
translates into. For instance, the following is true:

    sc(x1, modified_noun(adj(evil), noun(boy)), evil(x1) & boy(x1)).

# List of Files

* cpof.pl           - Main parsing/translation code.
* dictionary.pl     - Dictionary for the minimal language to be parsed.
* minimal_set.txt   - List of the minimum set of words required.
* phraseomat.py     - Test generation and testing script.
* read_in.pl        - Provided lexing code, copied verbatim.
* test.txt          - Hassler's test cases.

# Misc

`phraseomat.py` can automatically generate parsable sentences from nothing 
when you run:

    test_parse(False)
    
for several hours.
