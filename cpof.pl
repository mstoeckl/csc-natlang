%%%%%%%%%%%%%%%%%%%%
%    Setup stuff   %
%%%%%%%%%%%%%%%%%%%%

:- consult(read_in).
% add whatever additional dictionaries you want here.
:- consult(dictionary).

% AND and IMPLIES are the two main operators. Structure is always (A & D => B & C)
% precendences arbitrary and below |,|;| for safety
:- op(901, yfx, &).
:- op(902, xfy, =>).


%%%%%%%%%%%%%%%%%%%%
%   Parsing Code   %
%%%%%%%%%%%%%%%%%%%%

% The following parsing section is heavily inspired by the
% Clocksin/Mellish reading

sentence(sentence(X,Y)) --> noun_phrase(A,X), verb_phrase(A,Y), punctuation.

noun_phrase(A,noun_phrase(X,Y,Z)) --> determiner(E,A,X), anoun(E,A,Y), whom_clause(Z).
noun_phrase(A,noun_phrase(X,Y,Z)) --> determiner(E,A,X), anoun(E,A,Y), who_clause(A,Z).
noun_phrase(A,noun_phrase(X,Y)) --> determiner(E,A,X), anoun(E,A,Y).

whom_clause(rel_object_cl(X,Y)) --> whom_rel,noun_phrase(A,X), tverb(A,Y).
who_clause(A,rel_subject_cl(X)) --> who_rel, verb_phrase(A,X).

verb_phrase(A,verb_phrase(X)) --> everb(A,X).
verb_phrase(A,verb_phrase(X,Y)) --> tverb(A,X), noun_phrase(_,Y).
verb_phrase(A,verb_phrase(X,Y)) --> link(A,X), adj(_,Y).
verb_phrase(A,verb_phrase(X,Y)) --> link(A,X), noun_phrase(A,Y).% enforce #

everb(A,X) --> iverb(A,X); tverb(A,X).
anoun(E,A,X) --> noun(E,A,X).
anoun(E,A,modified_noun(X,Y)) --> adj(E,X),anoun(_,A,Y).


%%%%%%%%%%%%%%%%%%%%
% Translation Code %
%%%%%%%%%%%%%%%%%%%%

% short-name helper alias
ns(V) :- gensym('x', V).

% sentence with simple subject
convert(sentence(noun_phrase(forall, Y), Z), all(A, B => C)) :- ns(A), sc(A, Y, B), sc(A, Z, C).
convert(sentence(noun_phrase(exists, Y), Z), exists(A, B & C)) :- ns(A), sc(A, Y, B), sc(A, Z, C).

% sentence with complex subject
convert(sentence(noun_phrase(forall, Y, W), Z), all(A, B & D => C)) :- ns(A), sc(A, Y, B), sc(A, Z, C), sc(A, W, D).
convert(sentence(noun_phrase(exists, Y, W), Z), exists(A, B & D & C)) :- ns(A), sc(A, Y, B), sc(A, Z, C), sc(A, W, D).

% sc: single quant; structure; output

% simple noun
sc(Var, noun(U) , Z ) :- Z =.. [U,Var].
% modified noun
sc(Var, modified_noun(adj(U),V), A & B) :- sc(Var, V, A), B =.. [U, Var].

% rel_object_cl
sc(Var, rel_object_cl(noun_phrase(exists, U), verb(V)), exists(A, B & C)) :- ns(A),sc(A,U,B), C =.. [V, A, Var].
sc(Var, rel_object_cl(noun_phrase(forall, U), verb(V)), all(A, B => C)) :- ns(A),sc(A,U,B), C =.. [V, A, Var].
sc(Var, rel_object_cl(noun_phrase(exists, U, Z), verb(V)), exists(A, B & D & C)) :- ns(A),sc(A,U,B), C =.. [V, A, Var], sc(A, Z, D).
sc(Var, rel_object_cl(noun_phrase(forall, U, Z), verb(V)), all(A, B & D => C)) :- ns(A),sc(A,U,B), C =.. [V, A, Var], sc(A, Z, D).

% rel_subject_cl
sc(Var, rel_subject_cl(V), Z) :- sc(Var, V, Z).

% intransitive verb
sc(Var, verb_phrase(verb(U)), A) :- A =.. [U,Var].

% transitive verb and simple target
sc(Var, verb_phrase(verb(U), noun_phrase(forall, V)), all(A, B => C)) :- ns(A), sc(A, V, B), C =.. [U,Var, A].
sc(Var, verb_phrase(verb(U), noun_phrase(exists, V)), exists(A, B & C)) :- ns(A), sc(A, V, B), C =.. [U,Var, A].

% transitive verb and complex target
sc(Var, verb_phrase(verb(U), noun_phrase(forall, V, Z)), all(A, B & D => C)) :- ns(A), sc(A, V, B), C =.. [U,Var, A], sc(A, Z, D).
sc(Var, verb_phrase(verb(U), noun_phrase(exists, V, Z)), exists(A, B & D & C)) :- ns(A), sc(A, V, B), C =.. [U,Var, A], sc(A, Z, D).

% be verb and adjective
sc(Var, verb_phrase(be_verb, adj(V)), A) :- A =.. [V,Var].
sc(Var, verb_phrase(be_verb, noun_phrase(_, noun(V))), A) :- A =.. [V,Var].
sc(Var, verb_phrase(be_verb, noun_phrase(_, noun(V), W)), A & B) :- A =.. [V,Var], sc(Var, W, B).

%%%%%%%%%%%%%%%%%%%%
%     Interface    %
%%%%%%%%%%%%%%%%%%%%

% Necessary simplifiers
parse(X,V) :- sentence(V, X, []).
translate(X,V) :- parse(X,Y), convert(Y,V).

% test interfaces
test_read :- read_in(V), write(V),nl,halt;halt.
test_parse :- read_in(V), parse(V,X), write(X),nl,halt;halt.
test_translate :- read_in(V), translate(V,X), write(X),nl,halt;halt.